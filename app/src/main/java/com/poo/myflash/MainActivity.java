package com.poo.myflash;

import android.content.Context;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getName();
    private Button btSos, btPower;
    private CameraManager mCameraManager;
    private String mCameraId;
    private boolean isPowerOn;
    private Runnable runnable;
    private Thread thread;

    private boolean sosMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        btPower = findViewById(R.id.bt_power);
        btSos = findViewById(R.id.bt_sos);

        runnable = new Runnable() {
            @Override
            public void run() {
                execTask();
            }
        };
        thread = new Thread(runnable);
        btPower.setOnClickListener(this);
        btSos.setOnClickListener(this);
        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            mCameraId = mCameraManager.getCameraIdList()[0];
        } catch (Exception e) {
            e.getStackTrace();
        }


    }

    private void execTask() {
        int countTime = 0;
        while (countTime < 20) {
            countTime++;
            doFlash();
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                return;
            }
        }

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.bt_power) {
            if (isPowerOn) {
                btPower.setBackgroundResource(R.drawable.ic_power_on);
                doFlash();
            } else {

                btPower.setBackgroundResource(R.drawable.ic_power_off);
                btSos.setBackgroundResource(R.drawable.ic_sos_off);
            }
        } else {
            doSos();
        }
    }

    private void doSos() {
        if (!sosMode) {
            if (thread == null) {
                thread = new Thread(runnable);
            }
            sosMode = true;
            isPowerOn = false;
            thread.start();
            btPower.setBackgroundResource(R.drawable.ic_power_on);
            btSos.setBackgroundResource(R.drawable.ic_sos_on);
        } else {

            sosMode = false;
            isPowerOn = false;
            thread.interrupt();
            thread=null;
            btSos.setBackgroundResource(R.drawable.ic_sos_off);
        }
    }

    private void doFlash() {
        try {
            if (!isPowerOn) {
                mCameraManager.setTorchMode(mCameraId, !isPowerOn);
                isPowerOn = !isPowerOn;
            }
        } catch (Exception e) {
            e.getStackTrace();
        }


    }
}